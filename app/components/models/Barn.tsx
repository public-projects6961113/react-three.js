import React, { useContext, useState } from "react"
import { useGLTF } from "@react-three/drei"

import * as THREE from "three"
import { GLTF } from "three-stdlib"
import { Select } from "@react-three/postprocessing"
import { AlertContext } from "@/app/context/alert"

type GLTFResult = GLTF & {
  nodes: {
    Mesh: THREE.Mesh
    Mesh_1: THREE.Mesh
    Mesh_2: THREE.Mesh
    Mesh_3: THREE.Mesh
    Mesh_4: THREE.Mesh
    Mesh_5: THREE.Mesh
    Mesh_6: THREE.Mesh
    Mesh_7: THREE.Mesh
    Mesh001: THREE.Mesh
    Mesh001_1: THREE.Mesh
    Mesh001_2: THREE.Mesh
    LoftedBarn_6Wall_10x12_None_Wall1: THREE.Mesh
    LoftedBarn_6Wall_10x12_None_Wall2: THREE.Mesh
    LoftedBarn_6Wall_10x12_None_Wall3: THREE.Mesh
    LoftedBarn_6Wall_10x12_None_Wall4: THREE.Mesh
  }
  materials: {
    Siding_LPSmartPanelSiding: THREE.MeshStandardMaterial
    Siding_BoardandBatten: THREE.MeshStandardMaterial
    Roofing_Shingles_DesertTan: THREE.MeshStandardMaterial
    Wood_Trim_Interior: THREE.MeshStandardMaterial
    Wood_InteriorFloor: THREE.MeshStandardMaterial
    Wood_Trim: THREE.MeshStandardMaterial
    Metal_Interior: THREE.MeshStandardMaterial
    Metal_Exterior: THREE.MeshStandardMaterial
    Wood_Interior: THREE.MeshStandardMaterial
  }
}

const INITIAL_VALUES = new Array(4).fill(false)

export function Barn(props: JSX.IntrinsicElements["group"]) {
  const { setAlert } = useContext(AlertContext)
  const { nodes, materials } = useGLTF("/Barn_Testing.glb") as GLTFResult

  const [hoveredArray, setHoveredArray] = useState(INITIAL_VALUES)

  const handleOveredChange = (index: number, value: boolean, text: string) => {
    setHoveredArray(
      hoveredArray.map((item: boolean, i: number) => {
        if (i === index) return value

        return false
      })
    )
    setAlert(text)
  }
  return (
    <group {...props} dispose={null}>
      <group scale={[0.008, 0.011, 0.01]}>
        <mesh
          geometry={nodes.Mesh.geometry}
          material={materials.Siding_LPSmartPanelSiding}
        />
        <mesh
          geometry={nodes.Mesh_1.geometry}
          material={materials.Siding_BoardandBatten}
        />
        <mesh
          geometry={nodes.Mesh_2.geometry}
          material={materials.Roofing_Shingles_DesertTan}
        />
        <mesh
          geometry={nodes.Mesh_3.geometry}
          material={materials.Wood_Trim_Interior}
        />
        <mesh
          geometry={nodes.Mesh_4.geometry}
          material={materials.Wood_InteriorFloor}
        />
        <mesh geometry={nodes.Mesh_5.geometry} material={materials.Wood_Trim} />
        <mesh
          geometry={nodes.Mesh_6.geometry}
          material={materials.Metal_Interior}
        />
        <mesh
          geometry={nodes.Mesh_7.geometry}
          material={materials.Metal_Exterior}
        />
      </group>
      <group scale={[0.008, 0.011, 0.01]}>
        <mesh
          geometry={nodes.Mesh001.geometry}
          material={materials.Wood_Trim_Interior}
        />
        <mesh
          geometry={nodes.Mesh001_1.geometry}
          material={materials.Wood_Interior}
        />
        <mesh
          geometry={nodes.Mesh001_2.geometry}
          material={materials.Wood_Trim}
        />

        <Select enabled={hoveredArray[0]}>
          <mesh
            geometry={nodes.LoftedBarn_6Wall_10x12_None_Wall1.geometry}
            material={materials.Siding_LPSmartPanelSiding}
            onPointerOver={() =>
              handleOveredChange(0, true, "LoftedBarn_6Wall_10x12_None_Wall1")
            }
            onPointerOut={() =>
              handleOveredChange(0, false, "LoftedBarn_6Wall_10x12_None_Wall1")
            }
          />
        </Select>
        <Select enabled={hoveredArray[1]}>
          <mesh
            geometry={nodes.LoftedBarn_6Wall_10x12_None_Wall2.geometry}
            material={materials.Siding_LPSmartPanelSiding}
            onPointerOver={() =>
              handleOveredChange(1, true, "LoftedBarn_6Wall_10x12_None_Wall2")
            }
            onPointerOut={() =>
              handleOveredChange(1, false, "LoftedBarn_6Wall_10x12_None_Wall2")
            }
          />
        </Select>
        <Select enabled={hoveredArray[2]}>
          <mesh
            geometry={nodes.LoftedBarn_6Wall_10x12_None_Wall3.geometry}
            material={materials.Siding_LPSmartPanelSiding}
            onPointerOver={() =>
              handleOveredChange(2, true, "LoftedBarn_6Wall_10x12_None_Wall3")
            }
            onPointerOut={() =>
              handleOveredChange(2, false, "LoftedBarn_6Wall_10x12_None_Wall3")
            }
          />
        </Select>
        <Select enabled={hoveredArray[3]}>
          <mesh
            geometry={nodes.LoftedBarn_6Wall_10x12_None_Wall4.geometry}
            material={materials.Siding_LPSmartPanelSiding}
            onPointerOver={() =>
              handleOveredChange(3, true, "LoftedBarn_6Wall_10x12_None_Wall4")
            }
            onPointerOut={() =>
              handleOveredChange(3, false, "LoftedBarn_6Wall_10x12_None_Wall4")
            }
          />
        </Select>
      </group>
    </group>
  )
}

useGLTF.preload("/Barn_Testing.glb")
