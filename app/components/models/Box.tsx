import { useFrame } from "@react-three/fiber"
import { Select } from "@react-three/postprocessing"
import { useRef, useState } from "react"
import { Mesh } from "three"

export function Box(props: any) {
  const ref = useRef<Mesh>()
  const [hovered, hover] = useState(false)
  useFrame((state, delta) => {
    if (ref.current) ref.current.rotation.x = ref.current.rotation.y += delta
  })
  return (
    <Select enabled={hovered}>
      <mesh
        ref={ref}
        {...props}
        onPointerOver={() => hover(true)}
        onPointerOut={() => hover(false)}
      >
        <boxGeometry />
        <meshStandardMaterial color="blue" />
      </mesh>
    </Select>
  )
}
