import { useContext, useEffect, useState } from "react"
import { AlertContext } from "../context/alert"

export interface Alert {
  text: string
}
export function HoverAlert() {
  const { alert, setAlert } = useContext(AlertContext)
  const [position, setPosition] = useState("right-50 bottom-10")
  const [visible, setVisible] = useState(false)
  const [timeout, saveTimeout] = useState<NodeJS.Timeout>()

  useEffect(() => {
    if (timeout) {
      clearTimeout(timeout)
    }
    if (alert !== "") {
      setVisible(true)
      setPosition("right-10 bottom-10")
      const newTimeout = setTimeout(() => {
        setVisible(false)
      }, 2000)

      saveTimeout(newTimeout)
    }
  }, [alert])

  if (!visible) {
    return <></>
  }

  return (
    <div
      role="alert"
      className={`fixed ${position} p-2 bg-indigo-800 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex `}
    >
      <span className="flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">
        Selected
      </span>
      <span className="font-semibold mr-2 text-left flex-auto">{alert}</span>
    </div>
  )
}
