"use client"
import { Canvas, useThree } from "@react-three/fiber"
import { Box } from "./models/Box"
import { EffectComposer, Outline, Selection } from "@react-three/postprocessing"
import { Barn } from "./models/Barn"
import { useEffect, useState } from "react"

interface Window {
  height: number
  width: number
}
function Controls() {
  const {
    camera,
    gl: { domElement },
  } = useThree()
  return <orbitControls args={[camera, domElement]} />
}

export function Vizualizer() {
  const [{ height, width }, setWindow] = useState({} as Window)
  useEffect(() => {
    const { innerWidth, innerHeight } = window
    setWindow({ height: innerHeight, width: innerWidth })
  }, [])
  return (
    <div style={{ height, width }}>
      <Canvas>
        <ambientLight intensity={1} />
        <pointLight color="white" position={[5, 5, 5]} />
        <pointLight color="white" position={[-3, -3, 2]} />
        <pointLight color="white" position={[5, 5, -5]} />
        <Controls />

        <Selection>
          <EffectComposer multisampling={8} autoClear={false}>
            <Outline
              blur
              visibleEdgeColor={0xffffff}
              edgeStrength={100}
              width={1000}
            />
          </EffectComposer>

          <Barn position={[0, -1, 0]} />
        </Selection>
      </Canvas>
    </div>
  )
}
