import { useEffect, useState } from "react"

interface Props {
  text: string
  pos: number
}

export function IntroAlert({ text, pos }: Props) {
  const [visible, setVisible] = useState(true)
  const position = `bottom-${pos}`
  useEffect(() => {
    setTimeout(() => {
      setVisible(false)
    }, 10000)
  }, [])

  if (!visible) {
    return <></>
  }
  return (
    <div
      role="alert"
      className={`fixed ${position} right-50 p-2 bg-indigo-800 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex `}
    >
      <span className="flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">
        Info
      </span>
      <span className="font-semibold mr-2 text-left flex-auto">{text}</span>
    </div>
  )
}
