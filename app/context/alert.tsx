import React, { createContext, useState } from "react"

interface AlertContextProviderProps {
  children: React.ReactNode
}

const INITIAL_STATE: string = ""

const INITIAL_CONTEXT = {
  alert: "",
  setAlert: (alert: string) => {},
}

export const AlertContext = createContext(INITIAL_CONTEXT)

export const AlertContextProvider = ({
  children,
}: AlertContextProviderProps) => {
  const [alert, setAlert] = useState<string>(INITIAL_STATE)
  return (
    <AlertContext.Provider value={{ alert, setAlert }}>
      {children}
    </AlertContext.Provider>
  )
}
