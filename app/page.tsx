"use client"
import { HoverAlert } from "./components/HoverAlert"
import { IntroAlert } from "./components/IntroAlert"
import { Vizualizer } from "./components/Vizualizer"
import { AlertContextProvider } from "./context/alert"
import "./r3f-elements"

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <AlertContextProvider>
        <Vizualizer />
        <HoverAlert />
        <IntroAlert text="SCROLL UP TO ZOOM OUT" pos={9} />
        <IntroAlert text="SCROLL DOWN TO ZOOM IN" pos={10} />
      </AlertContextProvider>
    </main>
  )
}
