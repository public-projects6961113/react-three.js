This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## REALHOST RECRUITMENT TEST INSTRUCTIONS (spanish)

1. Cargar el modelo GLB con la librería
2. Generar la escena y escala correcta
3. Por medio de un dropdown o por otro medio se deben poder seleccionar las 4 paredes y resaltar la selección, (El modelo ya cuenta con los mesh necesarios)
4. La prueba debe realizarse en la última versión de React JS, se debe utilizar un paquete para compilación y de ser funcional al momento de compartirlo (Github, Gitlab o un repositorio publico)

## Structure & best practices

This project was developed under the course of a couple of hours, that's why there is a couple of details with it such as:

- Project file structure
- Incorrect use of development branch to write all code ( Didn't have time to use the Git Flow )

## Setup

- Clone the project using `git clone`
- Install dependencies using `npm i`
- Run dev server using:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

or look for the deployment on Vercel (If there's time for it)

## Author

Angel Gomez
